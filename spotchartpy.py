##################################################################################################
"""
name:				spotchartpy.py
author:				Felix Chan
email:				fmfchan@gmail.com
date created:			2020.02.22
description:			webscrapper for spotifycharts.com
"""
##################################################################################################
import datetime as dt
import bs4 as bs4
import pandas as pd
import requests as rq

def get_info(freq='daily', top='regional'):
    """
    Function to get all the available countries, country codes and dates
    Inputs:
        freq: str. daily or weekly
        top: regional or viral 
    Outputs:
        code: list. List of country codes. There are the values used in the URL.
        country: List. Names of cotunry. These are the values appear on the dropdown menu.
        date_value: List. All avalabile dates. These are the values used in the URL for scraping. 
        date_text: List. All available dates. These are the values appears on the dropdown list. 
    
    """
    suffix = '/'.join([top,'global',freq])
    base_url = 'https://spotifycharts.com/'
    f = rq.get(base_url+suffix)
    soup = bs4.BeautifulSoup(f.text, 'html.parser')
    code = []
    country = []
    date_value = []
    date_text = []
    ul = soup.find_all('ul')
    for i,u in enumerate(ul):
        for l in u.find_all('li'):
            if i == 0:
                code.append(l.get('data-value'))
                country.append(l.text)
            elif i==2:
                date_value.append(l.get('data-value'))
                date_text.append(l.text)
    return code,country,date_value,date_text

def webscrap_spotify(top='regional', freq='daily', country='global', date_value='latest', savefilename='test.csv'):
    """
    Webscrapping the top 200 tables in spotifycharts. 
    Inputs:
        freq: str. daily or weekly
        top: regional or viral 
        country: str. global or values from code from get_info. 
        date_value: str. Latest or values from date_value from get_info. 
        savefilename: csv file name for storing the data. 
    Output:
        top200: panda dataframe. 
    """
    base_url = 'https://spotifycharts.com/'
    suffix = '/'.join([top,country, freq, date_value])
    test = rq.get(base_url+suffix)
    if top == 'regional':
        test_pd = pd.read_html(test.text)[0].iloc[:,[3,4]]
    else:
        test_pd = pd.read_html(test.text)[0]
    soup = bs4.BeautifulSoup(test.text, 'html.parser')
    chart_table = soup.find_all('table', attrs={'class':'chart-table'})[0]
    id_table = chart_table.find_all('td', attrs={'class':'chart-table-image'})
    link_list = []
    id_list = []
    for i in id_table:
        temp = i.a.get('href')
        link_list.append(temp)
        id_list.append(temp.split('/')[-1])
    test_pd['link'] = link_list
    test_pd['id'] = id_list
    test_pd.to_csv(savefilename)
    return test_pd
